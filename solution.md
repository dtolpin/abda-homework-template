# Homework assignment X

## Question 1

### Problem 

Put the problem description here, including the exercise number in the book
if relevant.

### Solution

Write solution here. For formulas, use either plain text or LaTeX, for example

$p(x) = \mathcal{N}(x|\mu, \sigma)$

Use pandoc (http://pandoc.org/) to format the formulas to HTML or PDF if you
wish or/and a tool like upmath (https://upmath.me/) for visual editing. 

## Question 2

### Problem

### Solution

## Question 3

### Problem

### Solution
